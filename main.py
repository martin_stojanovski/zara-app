import os
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from Models.Statistics import Statistics
from Utilities.Utilities import Utilities

stats = Statistics()

scheduler = BackgroundScheduler()
scheduler.add_job(Utilities.send_statistics, CronTrigger.from_crontab('*/30 * * * *'), [stats])
scheduler.start()

importance = os.getenv('ZARA_IMPORTANCE', None)

if importance == None:
    Utilities.send_slack_message("Starting Testengine: None")
elif importance == 'HIGH':
    Utilities.send_slack_message("Starting Testengine: High importance")
elif importance == 'HIGH_2':
    Utilities.send_slack_message("Starting Testengine: High 2 importance")
elif importance == 'HIGH_3':
    Utilities.send_slack_message("Starting Testengine: High 3 importance")
elif importance == 'MEDIUM':
    Utilities.send_slack_message("Starting Testengine: Medium importance")

while True:
    stats.start_stats()

    if importance == None:
        os.system("python3 -m pytest ./Tests/step_defs/test_find_clothing.py -qq --tb=no --disable-pytest-warnings")
    elif importance == 'HIGH':
        os.system("python3 -m pytest ./Tests/step_defs/test_find_clothing_high_importance.py -qq --tb=no --disable-pytest-warnings")
    elif importance == 'HIGH_2':
        os.system("python3 -m pytest ./Tests/step_defs/test_find_clothing_high_2_importance.py -qq --tb=no --disable-pytest-warnings")
    elif importance == 'HIGH_3':
        os.system("python3 -m pytest ./Tests/step_defs/test_find_clothing_high_3_importance.py -qq --tb=no --disable-pytest-warnings")
    elif importance == 'MEDIUM':
        os.system("python3 -m pytest ./Tests/step_defs/test_find_clothing_medium_importance.py -qq --tb=no --disable-pytest-warnings")

    stats.add_time_now()
