
import time

class Statistics():
  def __init__(self):
    self.avg_time = 0
    self.runs = 0
    self.max_time = 0
    self.run_time = 0
  
  def start_stats(self):
    self.start = time.perf_counter()
  
  def init_stats(self):
    self.runs = 0
    self.max_time =  0
  
  def add_time_now(self):
    now = time.perf_counter()
    t = now - self.start
    self.avg_time += t
    self.avg_time /= 2

    if t > self.max_time:
      self.max_time = t

    self.start = time.perf_counter()
    self.runs +=1
  
  def __str__(self) -> str:
    return 'Max. Run Time: ' + str(int(self.max_time)) + 's' + ' Avg. Time: ' + str(int(self.avg_time))   +  's' + ' Runs: ' + str(self.runs)