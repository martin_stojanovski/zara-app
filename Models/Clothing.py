from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait

selector_1 = "//div[@class='product-detail-size-info__size']" 
selector_2 = "//span[@class='product-detail-size-info__main-label']"
selector_3 = "//div[@class='product-detail-size-info__show-similar']"
selector_4 = "//span[text()='M']/ancestor::li[@data-qa-action='size-in-stock']"


class Clothing():

    
    def __init__(self, driver, url=None, size=None):
        self.driver = driver
        if url != None:
            self.url = url
        if size != None:
            self.size = size

    @property
    def url(self):
        return self._url
    
    @url.setter
    def url(self, url):
        self._url = url

    @property
    def size(self):
        return self._size
    
    @size.setter
    def size(self, size):
        self._size = size.split(',') 
    
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    def size_available(self):
        self.driver.get(self.url)
       #print("Available sizes: "  + str(self.size))
        WebDriverWait(self.driver, 10).until(lambda x: x.find_element(By.XPATH, self._create_wait_selector()))

        print("Trying to print size")
        print("Sizes: " + str(self.size))

        for s in self.size :
            print("Checking size : " + s)
            if self._size_low_on_stock(s)  == True or self._size_in_stock(s) == True:
               #print("Available")
                return True 
            elif self._size_out_of_stock(s) == True:
               #print("Unavailable in " + s )
                continue

        #TODO:#print the data-qa-ation class into file for further usage
        print("Sizes " + str(self.size) + " not available!!")
        return False 

    def _size_out_of_stock(self,size):
            x_path_out_of_stock = self._create_selector_out_of_stock(size)
            #print("Checking " + size + " out of Stock!!")
            try:
                self.driver.find_element(By.XPATH, x_path_out_of_stock)
               #print("Size" + size + " out of Stock!!")
                return True
            except NoSuchElementException:
                return False

    def _size_in_stock(self,size):
            x_path_in_stock = self._create_selector_in_stock(size)
           #print("Checking " + size + " in of Stock!!")
            try:
                self.driver.find_element(By.XPATH, x_path_in_stock)
               #print("Size " + size + " in Stock!!")
                return True
            except NoSuchElementException:
                return False

    def _size_low_on_stock(self,size):
            x_path_low_on_stock = self._create_selector_low_on_stock(size)
           #print("Checking " + size + "low of Stock!!")
            try:
                self.driver.find_element(By.XPATH, x_path_low_on_stock)
               #print("Size " + size + "low on Stock!!")
                return True
            except NoSuchElementException:
                return False

    def _create_selector_in_stock(self, size):
        return  "//span[text()='" + size + "']/ancestor::li[@data-qa-action='size-in-stock']"

    def _create_selector_low_on_stock(self, size):
        return  "//span[text()='" + size + "']/ancestor::li[@data-qa-action='size-low-on-stock']"

    def _create_selector_out_of_stock(self, size):
        return  "//span[text()='" + size + "']/ancestor::li[@data-qa-action='size-out-of-stock']"

    def _create_wait_selector(self):
        return "//ul[@class='product-detail-size-selector__size-list']"

def validate_size(size):
    return True 