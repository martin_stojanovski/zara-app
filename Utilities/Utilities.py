

import requests
import os

from Tests.conftest import slack_channel_url


class Utilities():

    slack_channel_url = 'https://hooks.slack.com/services/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'

    def _send(message):
        slack_channel_url =  Utilities.slack_channel_url
        hostname = os.uname()[1]
        message = Utilities.create_message(hostname + ": " + message)
        requests.post(slack_channel_url, json = message)

    def create_message(message):
        d = dict()
        d['text'] = message
        return d
    
    def send_heartbeat():
        Utilities._send("Heartbeat message...")
    
    def send_statistics(stats):
        Utilities._send(str(stats))
        stats.init_stats()

    def send_slack_message(message): 
        Utilities._send(message)
