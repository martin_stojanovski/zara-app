
Feature: Find clothing
    Scenario Outline: Find the cloths
        Given   clothings name <name> is loaded
        And     clothings size <size> is loaded
        And     clothings url <url> is loaded

        When    clothing is in size available
        Then    send notification to slack channel 
    
        Examples:
        | name                                      | url                                                                                         | size |
        | STRETCHHEMD WEISS-2                       | https://www.zara.com/de/de/stretchhemd-p04488557.html                                       	| M  |
        | STRETCHHEMD WEISS-3                       | https://www.zara.com/de/de/stretchhemd-p07545402.html                                       	| M  |
        | STRETCHHEMD BLAU                          | https://www.zara.com/de/de/stretchhemd-p07545406.html                                       	| M  |
        | GESTREIFTES STRETCHHEMD                   | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545464.html                           	| M  |
        | GESTREIFTES STRETCHHEMD WEISS             | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545412.html                           	| M  |
        | GESTREIFTES STRETCHHEMD WEISS-B           | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545418.html                           	| M  |
        | GESTREIFTES STRETCHHEMD BLAU              | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545424.html                           	| M  |
        | GESTREIFTES STRETCHHEMD MARINEBLAU        | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545404.html                           	| M  |
        | Donegal Hose1                             | https://www.zara.com/de/de/donegal-anzughose-p04329275.html?v1=169912369           	      	| 40,42 |
        | Blazer Smoking                            | https://www.zara.com/de/de/smoking-anzugblazer-p04186118.html   				                | 52 |
        | Donegal Hose                              | https://www.zara.com/de/de/donegal-anzughose-p01564421.html           	      	            | 40,42 |
        | Donegal Blazer                            | https://www.zara.com/de/de/donegal-anzugblazer-p01564420.html           			            | 50 |
        | Hose  Strukturmuster1 2                   | https://www.zara.com/de/de/anzughose-mit-strukturmuster-p04534989.html    	              	| 40 |
        | Karrierter Hose                           | https://www.zara.com/de/de/karierte-anzughose-p05931786.html               			        | 42 |
        | Blazer Skinny FIT                         | https://www.zara.com/de/de/skinny-fit-anzugblazer-p04151419.html    				            | 50 |
        | Hose Skinny FI1                           | https://www.zara.com/de/de/anzughose-im-skinny-fit-p04152419.html   				            | 40 |
        | Hose Smoking1                             | https://www.zara.com/de/de/smoking-anzughose-p04187118.html 					                | 40 |
        | MANTEL WARM WOOL PREMIUM LIMITED EDI      | https://www.zara.com/de/de/mantel-warm-wool-premium-limited-edition-p02174747.html            | XS,S |
        | JACQUARD BLAZER - LIMITED EDITION 	    | https://www.zara.com/de/de/jacquard-blazer-%E2%80%93-limited-edition-p02881746.html		    | XS,S |
        | DOPPELREIHIGER BLAZER                     | https://www.zara.com/de/de/doppelreihiger-blazer-p02368782.html                               | XS,S |
        | DOPPELREIHIGER BLAZER - 1 S               | https://www.zara.com/de/de/doppelreihiger-blazer-p07901025.html                               | XS,S |
        | DOPPELREIHIGES JACKETT MIT STRUKTURMUSTER | https://www.zara.com/de/de/doppelreihiges-jackett-mit-strukturmuster-p09346460.html           | XS,S |
        | BLAZER MIT STRUKTURMUSTER UND KNÖPFEN S   | https://www.zara.com/de/de/blazer-mit-strukturmuster-und-knopfen-p02761062.html               | XS,S |
        | DOPPELREIHIGER BLAZER PINK - XS           | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02324687.html            | XS,S |
        | DOPPELREIHIGER BLAZER MIT HAHNENTRITTMUST | https://www.zara.com/de/de/doppelreihiger-blazer-mit-hahnentrittmuster-p02324509.html         | XS,S |

