
Feature: Find clothing
    Scenario Outline: Find the cloths
        Given   clothings name <name> is loaded
        And     clothings size <size> is loaded
        And     clothings url <url> is loaded

        When    clothing is in size available
        Then    send notification to slack channel 
    
        Examples:
        | name                                      | url                                                                                           | size |
        | KLEID MIT VOLANT UND SCHLITZEN            |   https://www.zara.com/de/de/kleid-mit-volant-und-schlitzen-p00264072.html                    | S |
        | KLEID MIT STICKEREI                       |   https://www.zara.com/de/de/kleid-mit-stickerei-p02157052.html                               | XS |
	#| JACQUARD-STRICKKLEID                      |   https://www.zara.com/de/de/jacquard-strickkleid-p02488029.html                              | S  |
        | STRICKKLEID MIT STREIFEN                  |   https://www.zara.com/de/de/strickkleid-mit-streifen-p03991014.html                          | S  |
        | POINTELLE-STRICKKLEID                     |   https://www.zara.com/de/de/pointelle-strickkleid-p02893009.html                             | XS,S,M  |
        | MINIKLEID MIT STICKEREI                   |   https://www.zara.com/de/de/minikleid-mit-stickerei-p04387076.html                           | S,XS |
        | LANGER PLISSIERTER OVERALL                |   https://www.zara.com/de/de/langer-plissierter-overall-p09878095.html                        | XS,S |
        | JOGGINGHOSE MIT TASCHEN                   |   https://www.zara.com/de/de/jogginghose-mit-taschen-p02969077.html                           | XS |
        | POPELINKLEID MIT ELASTISCHER TAILLE       |   https://www.zara.com/de/de/popelinkleid-mit-elastischer-taille-p02298094.html               | S,XS |
        | KLEID AUS FESTEM STOFF MIT KNITTEROPTIK   |   https://www.zara.com/de/de/kleid-aus-festem-stoff-mit-knitteroptik-p02298068.html           | S,XS |
        | SATINIERTER NECKHOLDER-OVERALL            |   https://www.zara.com/de/de/satinierter-neckholder-overall-p09878096.html                    | S,XS |
        | TUNIKAKLEID MIT GÃœRTEL                   |   https://www.zara.com/de/de/tunikakleid-mit-gurtel-p02298066.html                            | S,XS |
        | KURZES KLEID MIT SCHMUCKSTEINEN           |   https://www.zara.com/de/de/kurzes-kleid-mit-schmucksteinen-p05039035.html                   | S  |
        | KLEID MIT STRUKTURMUSTER UND CUT-OUT      |   https://www.zara.com/de/de/kleid-mit-strukturmuster-und-cut-out-p02643523.html              | S  |
        | MIDIKLEID MIT LOCHMUSTER                  |   https://www.zara.com/de/de/midikleid-mit-lochmuster-p03575919.html                          | XS |
        | MIDIKLEID MIT LOCHMUSTER                  |   https://www.zara.com/de/de/midikleid-mit-lochmuster-p03575919.html                          | S  |
        | GEMUSTERTER ROCK MIT SCHMUCKPERLEN        |   https://www.zara.com/de/de/gemusterter-rock-mit-schmuckperlen-p01821025.html                | S,XS |
        | MITTELLANGES HEMDBLUSENKLEID              |   https://www.zara.com/de/de/mittellanges-hemdblusenkleid-p03081022.html                      | S,XS |
        | KURZES KLEID MIT PRINT                    |   https://www.zara.com/de/de/kurzes-kleid-mit-print-p03637238.html                            | S,XS |
        | DRAPIERTES KLEID MIT VOLANTÃ„RMELN        |   https://www.zara.com/de/de/drapiertes-kleid-mit-volantarmeln-p02934028.html                 | S,XS |
        | JACKE MIT RAFFUNGEN UND KNOPFLEISTE       |   https://www.zara.com/de/de/jacke-mit-raffungen-und-knopfleiste-p05580007.html               | S,XS  |
        | SATINIERTES KORSETT-TOP IM CROP-SCHNITT   |   https://www.zara.com/de/de/satiniertes-korsett-top-im-crop-schnitt-p03236840.html           | XS,S  |
        | DOPPELREIHIGER BLAZER MIT SPITZE          |   https://www.zara.com/de/de/doppelreihiger-blazer-mit-spitze-p02496912.html                  | S,XS |
        | LANGE HOSE MIT SPITZE                     |   https://www.zara.com/de/de/lange-hose-mit-spitze-p02899912.html?v1=164685984                | S,XS |
        | BUSTIER MIT MARGERITEN                    |   https://www.zara.com/de/de/bustier-mit-margeriten-p08779143.html                            | S,XS |
        | OVERALL MIT KNITTER-EFFEKT UND GÃœRTEL    |   https://www.zara.com/de/de/overall-mit-knitter-effekt-und-gurtel-p09878101.html             | S,XS |
        | HALBTRANSPARENTES HEMD MIT STICKEREI      |   https://www.zara.com/de/de/halbtransparentes-hemd-mit-stickerei-p06895611.html              | S,XS  |
        | HÃ„KELJACKE                               |   https://www.zara.com/de/de/hakeljacke-p08741044.html                                        | XS,S |
        | HEMDBLUSENKLEID MIT BLUMENMUSTER          |   https://www.zara.com/de/de/hemdblusenkleid-mit-blumenmuster-p03563244.html                  | XS,S |
        | SATINIERTE HOSE â€“ FULL LENGTH           |   https://www.zara.com/de/de/satinierte-hose-%E2%80%93-full-length-p07385467.html             | XS |
        | FLIESSENDE FLARE-HOSE                     |   https://www.zara.com/de/de/fliessende-flare-hose-p03111117.html                             | XS |
        | STRICK-TOP MIT GOLDFARBENER SCHNALLE      |   https://www.zara.com/de/de/strick-top-mit-goldfarbener-schnalle-p04938005.html              | S  |
        | HÃ„KELKLEID MIT CUT-OUT  LIMITED EDITION  |   https://www.zara.com/de/de/hakelkleid-mit-cut-out-%E2%80%93-limited-edition-p02142313.html  | S,XS  |
        | MINIROCK MIT STRUKTUR                     |   https://www.zara.com/de/de/minirock-mit-struktur-p01971078.html                             | XS |
	| DRAPIERTES PLUMETIS-KLEID		    |	https://www.zara.com/de/de/drapiertes-plumetis-kleid-p07589825.html 			    | XS,S |
	| HAEKELKLEID MIT FRANSENDETAIlS            |   https://www.zara.com/de/de/hakelkleid-mit-fransendetails-%E2%80%93-limited-edition-p01957004.html | S |
	| STRICKJACKE IN KUNSTFELLOPTIK		    | 	https://www.zara.com/de/de/strickjacke-in-kunstfelloptik-p06873156.html				| XS,S |


