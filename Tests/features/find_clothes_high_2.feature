
Feature: Find clothing
    Scenario Outline: Find the cloths
        Given   clothings name <name> is loaded
        And     clothings size <size> is loaded
        And     clothings url <url> is loaded

        When    clothing is in size available
        Then    send notification to slack channel 
    
        Examples:
        | name                                      | url                                                                                               | size |
        | KLEID MIT PRINT UND LAMÉFADEN             | https://www.zara.com/de/de/kleid-mit-print-und-lam-faden-p03047184.html                           | XS,S,M |
        | KLEID AUS LEINENMISCHGEWEBE STEINEN       | https://www.zara.com/de/de/kleid-aus-leinenmischgewebe-mit-steinen-p08372105.html                 | XS,S,M |       
        | MANTEL WARM WOOL PREMIUM LIMITED EDI      | https://www.zara.com/de/de/mantel-warm-wool-premium-limited-edition-p02174747.html                | XS,S,M |
        | Taillierter Blazer mit Strukturmuster     | https://www.zara.com/de/de/taillierter-blazer-mit-strukturmuster-p02905481.html                   | XS,S,M |
        | BLAZER IN SATINOPTIK MIT SCHLEIFE         | https://www.zara.com/de/de/blazer-in-satinoptik-mit-schleife-p03552260.html	                    | XS,S,M |
        | BLAZER AUS KUNSTLEDER                     | https://www.zara.com/de/de/blazer-aus-kunstleder-p04432710.html                                   | XS,S,M |     
        | BLAZER MIT SMOKINGKRAGEN                  | https://www.zara.com/de/de/blazer-mit-smokingkragen-p02766178.html                                | XS,S,M |
        | BLAZER MIT STRUKTURMUSTER                 | https://www.zara.com/de/de/blazer-mit-strukturmuster-und-knopfen-p02761051.html                   | XS,S,M |
        | BLAZER MIT STRUKTURMUSTERGOLDKNOPF        | https://www.zara.com/de/de/blazer-mit-strukturmuster-und-goldknopf-p02324480.html                 | XS,S,M |
        | DOPPELREIHIGER BLAZER ROSA                | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p07553147.html                | XS,S,M |
        | DOPPELREIHIGER BLAZER ROSA 22             | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02324133.html                | XS,S,M |
        | DOPPELREIHIGER BLAZER GRUEN               | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02387510.html                | XS,S,M |
        | DOPPELREIHIGER BLAZER WEISS               | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02324144.html                | XS,S,M |
        | DOPPELREIHIGER BLAZER PINK - XS           | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02324687.html                | XS,S,M |
        | BUSTIER-KLEID LIMITED EDITION             | https://www.zara.com/de/de/bustier-kleid-limited-edition-p09644500.html                           | XS,S,M |
        | SMOKING-KLEID LIMITED EDITION             | https://www.zara.com/de/de/smoking-kleid-limited-edition-p02674898.html                           | XS,S,M |
        | HAEKELKLEID LIMITED EDITION               | https://www.zara.com/de/de/hakelkleid-mit-fransendetails-%E2%80%93-limited-edition-p01957004.html | XS,S,M |
        | WEITES KLEID LIMITED EDITION              | https://www.zara.com/de/de/weites-kleid-limited-edition-p09646495.html                            | XS,S,M |
	    | JACQUARD BLAZER - LIMITED EDITION 	    | https://www.zara.com/de/de/jacquard-blazer-%E2%80%93-limited-edition-p02881746.html		        | XS,S,M |
        | DOPPELREIHIGER BLAZER                     | https://www.zara.com/de/de/doppelreihiger-blazer-p02368782.html                                   | XS,S,M |
        | DOPPELREIHIGER BLAZER - 1 S               | https://www.zara.com/de/de/doppelreihiger-blazer-p07901025.html                                   | XS,S,M |
        | DOPPELREIHIGES JACKETT MIT STRUKT         | https://www.zara.com/de/de/doppelreihiges-jackett-mit-strukturmuster-p09346460.html               | XS,S,M |
        | BLAZER MIT STRUKTURMUSTER UND KNÖPFEN S   | https://www.zara.com/de/de/blazer-mit-strukturmuster-und-knopfen-p02761062.html                   | XS,S,M |
        | DOPPELREIHIGER BLAZER MIT HAHNENTRITTMU   | https://www.zara.com/de/de/doppelreihiger-blazer-mit-hahnentrittmuster-p02324509.html             | XS,S,M |
        | BLAZER MIT FEDERN  LIMITED EDITION        | https://www.zara.com/de/de/blazer-mit-federn-%E2%80%93-limited-edition-p03308839.html             | XS,S |
        | SATINIERTE PYJAMAHOSE                     | https://www.zara.com/de/de/satinierte-pyjamahose-p03132839.html?v1=184942175                      | XS,S |
        | GEMUSTERTES HEMD MIT TASCHE               | https://www.zara.com/de/de/gemustertes-hemd-mit-tasche-p03156234.html                             | XS,S |
        | SATINIERTE BLUSE MIT PRINT                | https://www.zara.com/de/de/satinierte-bluse-mit-print-p04886069.html                              | XS,S |
        | BLAZER IN SATINOPTIK MIT GÜRTEL           | https://www.zara.com/de/de/blazer-in-satinoptik-mit-gurtel-p03357685.html                         | XS,S |
        | TAILLIERTER DOPPELREIHIGER BLAZER         | https://www.zara.com/de/de/taillierter-doppelreihiger-blazer-p02428742.html                       | XS,S,M |
        | GERADE GESCHNITTENER BLAZER MIT GÜRTEL    | https://www.zara.com/de/de/gerade-geschnittener-blazer-mit-gurtel-p02010702.html                  | XS,S |
        | GERADE GESCHNITTENER BLAZER MIT TASCHEN   | https://www.zara.com/de/de/gerade-geschnittener-blazer-mit-taschen-p02761057.html                 | XS,S,M |
        

