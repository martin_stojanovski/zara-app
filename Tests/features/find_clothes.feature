
Feature: Find clothing
    Scenario Outline: Find the cloths
        Given   clothings name <name> is loaded
        And     clothings size <size> is loaded
        And     clothings url <url> is loaded

        When    clothing is in size available
        Then    send notification to slack channel 
    
        Examples:
        | name                                  | url                                                                                         | size |
        | Taillierter Blazer mit Strukturmuster | https://www.zara.com/de/de/taillierter-blazer-mit-strukturmuster-p02905481.html             | XS |
        | Gemustertes Kleid mit Cut-Out         | https://www.zara.com/de/de/gemustertes-kleid-mit-cut-out-p03548238.html                     | XS |
        | Kleid mit Lochstickerei               | https://www.zara.com/de/de/kleid-mit-lochstickerei-p03211330.html                           | XS |
#        | Blazer in Satinoptik mit Schleife     | https://www.zara.com/de/de/blazer-in-satinoptik-mit-schleife-p03552260.html                 | XS |
#        | Taillierter Blazer mit Strukturmuster | https://www.zara.com/de/de/taillierter-blazer-mit-strukturmuster-p02905481.html             | XS |
#        | MIDIKLEID MIT LOCHMUSTER              | https://www.zara.com/de/de/midikleid-mit-lochmuster-p07686992.html                          | XS |
#        | GEMUSTERTE BLUSE                      | https://www.zara.com/de/de/gemusterte-bluse-p02664188.html                                  | XS |
#        | MAKRAMEE-ROCK		                | https://www.zara.com/de/de/makramee-rock-p01014505.html                                     | S  |
#        | GERADE GESCHNITTENER HÄKELROCK        | https://www.zara.com/de/de/gerade-geschnittener-hakelrock-p08741051.html                    | XS |
#        | HÄKELHOSE                             | https://www.zara.com/de/de/hakelhose-p08741223.html                                         | XS |
#        | SATINIERTES HEMD MIT BLUMENMUSTER     | https://www.zara.com/de/de/satiniertes-hemd-mit-blumenmuster-p02761162.html                 | XS |
#        | SATINIERTE HOSE                       | https://www.zara.com/de/de/satinierte-hose-p03560260.html                                   | XS |
#        | BLAZER IN SATINOPTIK MIT SCHLEIFE     | https://www.zara.com/de/de/blazer-in-satinoptik-mit-schleife-p03552260.html	              | XS | 
#        | HEMDBLUSENKLEID MIT LOCHSTICKEREI     | https://www.zara.com/de/de/hemdblusenkleid-mit-lochstickerei-p03051922.html                 | XS |
#	#| STRETCHHEMD WEISS                     | https://www.zara.com/de/de/stretchhemd-p04073612.html                                       | L  |
#        #| STRETCHHEMD WEISS-2                   | https://www.zara.com/de/de/stretchhemd-p04488557.html                                       | L  |
#        #| STRETCHHEMD BLAU                      | https://www.zara.com/de/de/stretchhemd-p07545406.html                                       | L  |
#        #| GESTREIFTES STRETCHHEMD WEISS         | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545412.html                           | L  |
#        #| GESTREIFTES STRETCHHEMD WEISS-B       | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545418.html                           | L  |
#        #| GESTREIFTES STRETCHHEMD               | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545464.html                           | L  |
#        #| GESTREIFTES STRETCHHEMD BLAU          | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545424.html                           | L  |
#        | STRETCHHEMD WEISS                     | https://www.zara.com/de/de/stretchhemd-p04073612.html                                       | M  |
#        | STRETCHHEMD WEISS-2                   | https://www.zara.com/de/de/stretchhemd-p04488557.html                                       | M  |
#        | STRETCHHEMD WEISS-3                   | https://www.zara.com/de/de/stretchhemd-p07545402.html                                       | M  |
#        | STRETCHHEMD WEISS                     | https://www.zara.com/de/de/stretchhemd-p04073612.html                                       | S  |
#        | STRETCHHEMD WEISS-2                   | https://www.zara.com/de/de/stretchhemd-p04488557.html                                       | S  |
#        | STRETCHHEMD WEISS-3                   | https://www.zara.com/de/de/stretchhemd-p07545402.html                                       | S  |
#        | STRETCHHEMD BLAU                      | https://www.zara.com/de/de/stretchhemd-p07545406.html                                       | M  |
#        | GESTREIFTES STRETCHHEMD               | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545464.html                           | M  |
#        | GESTREIFTES STRETCHHEMD WEISS         | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545412.html                           | M  |
#        | GESTREIFTES STRETCHHEMD WEISS-B       | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545418.html                           | M  |
#        | GESTREIFTES STRETCHHEMD BLAU          | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545424.html                           | M  |
#        | GESTREIFTES STRETCHHEMD MARINEBLAU    | https://www.zara.com/de/de/gestreiftes-stretchhemd-p07545404.html                           | M  |
#        | BLAZER AUS KUNSTLEDER                 | https://www.zara.com/de/de/blazer-aus-kunstleder-p04432710.html                             | XS |       
#        | BLAZER MIT SMOKINGKRAGEN              | https://www.zara.com/de/de/blazer-mit-smokingkragen-p02766178.html                          | XS |
#        | FLIESSENDE FLARE-HOSE                 | https://www.zara.com/de/de/fliessende-flare-hose-p03111117.html?v1=170030931                | XS |
#        | HEMDBLUSENKLEID MIT LOCHSTICKEREI     | https://www.zara.com/de/de/hemdblusenkleid-mit-lochstickerei-p03051922.html                 | XS |
#        | MIDIKLEID MIT LOCHMUSTER 	        | https://www.zara.com/de/de/midikleid-mit-lochmuster-p07686992.html                          | XS |
#        | MIDIKLEID MIT LOCHMUSTER 	        | https://www.zara.com/de/de/midikleid-mit-lochmuster-p07686992.html                          | S  |
#        | GEMUSTERTES KLEID MIT CUT-OUT         | https://www.zara.com/de/de/gemustertes-kleid-mit-cut-out-p03548238.html                     | S  |
#        | SATINIERTES KLEID                     | https://www.zara.com/de/de/satiniertes-kleid-p02907062.html		                      | XS |
#        | SATINIERTES KLEID                     | https://www.zara.com/de/de/satiniertes-kleid-p02907062.html		                      | S  |
#        | ASYMMETRISCHES STRICKTOP MIT SCHMUCK  | https://www.zara.com/de/de/asymmetrisches-stricktop-mit-schmuckperlen-p03920068.html        | S  |
#        | HEMD MIT HALSTUCHPRINT                | https://www.zara.com/de/de/hemd-mit-halstuchprint-p02510258.html                            | XS |
#        | SHORTS MIT HOHEM BUND                 | https://www.zara.com/de/de/shorts-mit-hohem-bund-p04886057.html                             | XS |
#        | GEMUSTERTES KLEID MIT GÃRTEL          | https://www.zara.com/de/de/gemustertes-kleid-mit-gurtel-p03666118.html                      | XS |
#        | SATINIERTE BLUSE MIT PRINT            | https://www.zara.com/de/de/satinierte-bluse-mit-print-p04886069.html                        | XS |
#        | MANTEL MIT HOHEM WOLL                 | https://www.zara.com/de/de/mantel-mit-hohem-kragen-aus-wollmischgewebe-p02096289.html       | XS |
#        | BLAZER MIT STRUKTURMUSTER             | https://www.zara.com/de/de/blazer-mit-strukturmuster-und-knopfen-p02761051.html             | XS |
#        | GEBTES KLEID MIT LEINENANTEIL         | https://www.zara.com/de/de/geblumtes-kleid-mit-leinenanteil-p00881332.html                  | XS |
#        | KURZES KLEID MIT VOLANT               | https://www.zara.com/de/de/kurzes-kleid-mit-volant-p03277921.html                           | XS |
#        | HKELKLEID  LIMITED EDITION            | https://www.zara.com/de/de/hakelkleid-%E2%80%93-limited-edition-p01957005.html              | S  |
#        | KLEID IN SATINOPTIK  ANIMALPRINT      | https://www.zara.com/de/de/kleid-in-satinoptik-mit-cut-out-und-animalprint-p02492072.html   | XS | 
#        | KLEID IN SATINOPTIK  ANIMALPRINT      | https://www.zara.com/de/de/kleid-in-satinoptik-mit-cut-out-und-animalprint-p02492072.html   | S  |
#        | LANGER, BESTICKTER ROCK               | https://www.zara.com/de/de/langer--bestickter-rock-p03339913.html                           | XS |
#        | LANGER, BESTICKTER ROCK               | https://www.zara.com/de/de/langer--bestickter-rock-p02731051.html                           | XS |
#        | KLEID MIT PRINT UND LAMÉFADEN         | https://www.zara.com/de/de/kleid-mit-print-und-lam-faden-p03047184.html                     | XS |
#        | KLEID AUS FESTEM IN LINGERIE-OPTIK    | https://www.zara.com/de/de/kleid-aus-festem-stoff-in-lingerie-optik-p03564083.html          | XS |
#        | SATINIERTES KLEID                     | https://www.zara.com/de/de/satiniertes-kleid-p02907062.html                                 | S  |
#        | NECKHOLDER-BODY RIB                   | https://www.zara.com/de/de/neckholder-body-rib-p00264545.html                               | S  |
#        | DRAPIERTES WICKELKLEID                | https://www.zara.com/de/de/drapiertes-wickelkleid-p03152376.html                            | S  |
#        | BLAZER MIT STRUKTURMUSTERGOLDKNOPF    | https://www.zara.com/de/de/blazer-mit-strukturmuster-und-goldknopf-p02324480.html           | XS |
#        | CORSAGENTOP MIT TÜLL UND PRINT        | https://www.zara.com/de/de/corsagentop-mit-tull-und-print-p05039649.html                    | S  |
#        | BESTICKTES KLEID ELASTISCHER TAILLE   | https://www.zara.com/de/de/besticktes-kleid-mit-elastischer-taille-p07696993.html           | XS |
#        | KLEID LINGERIE-O MIT KETTENDETAILS    | https://www.zara.com/de/de/kleid-in-lingerie-optik-mit-kettendetails-p03186845.html         | XS |
#        | MANTEL WARM WOOL PREMIUM LIMITED EDI  | https://www.zara.com/de/de/mantel-warm-wool-premium-limited-edition-p02174747.html          | XS |
#        | FLIESSENDE FLARE-HOSE                 | https://www.zara.com/de/de/fliessende-flare-hose-p03111117.html                             | XS |
#        | SATINIERTE BLUSE MIT PRINT            | https://www.zara.com/de/de/satinierte-bluse-mit-print-p04886069.html                        | XS |
#        | GEMUSTERTES HEMD MIT TASCHE           | https://www.zara.com/de/de/gemustertes-hemd-mit-tasche-p03156234.html                       | XS |
#        | HEMD MIT HALSTUCHPRINT                | https://www.zara.com/de/de/hemd-mit-halstuchprint-p02510258.html                            | XS |
#        | SHORTS AUS LEINENMISCHUNG MIT PRINT   | https://www.zara.com/de/de/shorts-aus-leinenmischung-mit-print-p03340579.html               | XS |
#        | BIKINIHOSE MIT KREUZRIEMCHEN          | https://www.zara.com/de/de/bikinihose-mit-kreuzriemchen-p02910004.html                      | S  | 
#        | BIKINI-TOP MIT CUT-OUT                | https://www.zara.com/de/de/bikini-top-mit-cut-out-p02910003.html?v1=157560724               | S  | 
#        | KLEID MIT PRINT UND LAMÉFADEN         | https://www.zara.com/de/de/kleid-mit-print-und-lam-faden-p03047184.html                     | XS |
#        | LANGES KLEID MIT PRINT                | https://www.zara.com/de/de/langes-kleid-mit-print-p03221203.html                            | XS |
#        | DOPPELREIHIGER BLAZER MIT STRUKTURMUS | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p07553147.html          | XS |
#        | DOPPELREIHIGER BLAZER MIT STRUKTURMUS | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02324133.html          | XS |
#        | DOPPELREIHIGER BLAZER MIT STRUKTURMUS | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02387510.html          | XS |
#        | MINIROCK MIT STRUKTUR                 | https://www.zara.com/de/de/minirock-mit-struktur-p01971078.html                             | XS |
#        | TAILLIERTER DOPPELREIHIGER BLAZER     | https://www.zara.com/de/de/taillierter-doppelreihiger-blazer-p02443148.html                 | XS |
#        | SATINIERTES HEMD MIT PRINT            | https://www.zara.com/de/de/satiniertes-hemd-mit-print-p09006151.html                        | XS |
#        | SOFT-OBERTEIL MIT RIPPENMUSTER UND    | https://www.zara.com/de/de/soft-oberteil-mit-rippenmuster-und-polokragen-p01131971.html     | S  | 
#        | SATINIERTE BLUSE MIT PRINT            | https://www.zara.com/de/de/satinierte-bluse-mit-print-p04886069.html                        | XS |
#        | DOPPELREIHIGER BLAZER MIT STRUKTURM   | https://www.zara.com/de/de/doppelreihiger-blazer-mit-strukturmuster-p02324144.html          | XS |
#        | NECKHOLDER-OBERTEIL MIT RIPPENMUSTER  | https://www.zara.com/de/de/neckholder-oberteil-mit-rippenmuster-p00858326.html              | M  | 
#        | KLEID AUS LEINENMISCHGEWEBE STEINEN   | https://www.zara.com/de/de/kleid-aus-leinenmischgewebe-mit-steinen-p08372105.html           | XS |
#        | MANTEL MIT HOHEM  WOLLMISCHGEWEBE     | https://www.zara.com/de/de/mantel-mit-hohem-kragen-aus-wollmischgewebe-p02096289.html       | XS |
        #| Elastisches Popelinhemd               | https://www.zara.com/de/de/elastisches-popelinhemd-p05588644.html                          | S  |
	#| SATINIERTE HOSE  FULL LENGTH          | https://www.zara.com/de/de/satinierte-hose-%E2%80%93-full-length-p07385467.html            | XS |
	#| DRAPIERTER NECKHOLDER-BODY            | https://www.zara.com/de/de/drapierter-neckholder-body-p04770013.html                       | S  |
	#| KLEID MIT PATCHWORK-PRINT             | https://www.zara.com/de/de/kleid-mit-patchwork-print-p02764147.html                        | XS |
	#| HEMDBLUSENKLEID MIT LOCHSTICKEREI     | https://www.zara.com/de/de/hemdblusenkleid-mit-lochstickerei-p03051922.html                | S  |
	#| KURZER GEMUSTERTER ROCK               | https://www.zara.com/de/de/kurzer-gemusterter-rock-p02789157.html                          | S  |
        #| GERADE GESCHNITTENER HÄKELROCK        | https://www.zara.com/de/de/gerade-geschnittener-hakelrock-p08741051.html                   | S  |




