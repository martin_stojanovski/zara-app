

from Utilities.Utilities import Utilities

def test_create_message_simple():
    d = dict()
    d['text'] = "Hello, World!"

    expected = d

    actual = Utilities.create_message("Hello, World!")

    assert str(expected) == str(actual)

    