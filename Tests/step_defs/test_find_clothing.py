from email import parser
from pytest_bdd import scenarios, given, when, then, parsers
import requests

from Models.Clothing import Clothing
from Utilities.Utilities import Utilities

scenarios("../features/find_clothes.feature")

# Given

@given(parsers.parse("clothings url {url} is loaded"))
def load_clothing_url(clothing, url):
    clothing.url = url 
    print("URL loaded successful: " + clothing.url)
    
@given(parsers.parse("clothings name {name} is loaded"))
def load_clothing_name(clothing, name):
    clothing.name = name 
    print("Name loaded successful: " + clothing.name)

@given(parsers.parse("clothings size {size} is loaded"))
def load_clothing_size(clothing, size):
    clothing.size = size 
    print("Size loaded successful: " + clothing.size)

@when(parsers.parse("clothing is in size available"))
def is_size_available(clothing):
    if clothing.size_available() is False:
        assert False, 'Clothing ' + clothing.name + ' is in Size ' + clothing.size + ' unavailable!'
    

@then(parsers.parse("send notification to slack channel"))
def send_email(clothing, slack_channel_url):
    message = Utilities.create_message(f'{clothing.name} ({clothing.url }) is available in size {clothing.size} ')
    requests.post(slack_channel_url, json = message)
