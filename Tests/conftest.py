import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from Models.Clothing import Clothing

@pytest.fixture(scope="module")
def driver():
    options = Options()
    options.headless = True 
    d  = webdriver.Firefox(options=options, executable_path='./geckodriver')
    yield d 
    d.quit()

@pytest.fixture
def clothing(driver):
    c = Clothing(driver)
    yield c

@pytest.fixture
def slack_channel_url():
    return 'https://hooks.slack.com/services/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'