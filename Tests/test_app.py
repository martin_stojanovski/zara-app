import pytest
from Models.Clothing import Clothing

def sample_url():
    return "https://www.zara.com/de/de/elastisches-popelinhemd-p05588644.html"

def sample_url_in_stock():
    return "https://www.zara.com/de/de/elastisches-popelinhemd-p05588644.html"

def sample_url_low_on_stock():
    return "https://www.zara.com/de/de/steppjacke-mit-kapuze-p07522045.html"

@pytest.mark.skip
def test_size_available(driver):
    expected = True
    url = sample_url()

    shirt = Clothing(driver, url, 'S')
    
    actual = shirt.size_available()

    assert expected == actual

@pytest.mark.skip
def test_size_in_stock(driver):
    expected = True
    url = sample_url_in_stock()

    shirt = Clothing(driver, url, 'S')
    
    actual = shirt.size_available()

    assert expected == actual

@pytest.mark.skip
def test_size_low_on_stock(driver):
    expected = True
    url = sample_url_low_on_stock()

    shirt = Clothing(driver, url, 'L')
    
    actual = shirt.size_available()

    assert expected == actual

@pytest.mark.skip
def test_size_available_1(driver):
    expected = False 
    url = sample_url()

    shirt = Clothing(driver, url, 'XS')
    actual = shirt.size_available()

    assert expected == actual

@pytest.mark.skip
def test_multi_size(driver):
    expected_sizes = ['S', 'XS', 'M'] 
    url = sample_url()

    shirt = Clothing(driver, url, 'S,XS,M')
    
    actual_sizes = shirt.size

    assert expected_sizes == actual_sizes 

def test_load_size(driver):
    expected = True 
    url = sample_url()

    shirt = Clothing(driver)
    shirt.url = 'https://www.zara.com/de/de/elastisches-popelinhemd-p05588644.html'
    shirt.size = 'XS,L,M'
    
    actual = shirt.size_available()

    assert expected == actual

@pytest.mark.skip
def test_multi_size_available(driver):
    expected = True 
    url = sample_url()

    shirt = Clothing(driver, url, 'XS,L')
    
    actual = shirt.size_available()

    assert expected == actual